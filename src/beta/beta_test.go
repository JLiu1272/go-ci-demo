package main

import (
	"fmt"
	"testing"
)

type testCase struct {
	a   int
	b   int
	ans int
}

func TestAdd(t *testing.T) {
	addCases := []testCase{
		{1, 1, 2},
		{2, 3, 5},
	}
	for _, tc := range addCases {
		t.Run(fmt.Sprintf("%d + %d", tc.a, tc.b), func(t *testing.T) {
			res := add(tc.a, tc.b)
			if res != tc.ans {
				t.Errorf("%d + %d = %d, expected %d", tc.a, tc.b, res, tc.ans)
			}
		})
	}
}

func TestTimes(t *testing.T) {
	timesCases := []testCase{
		{4, 4, 16},
		{5, 9, 45},
	}
	for _, tc := range timesCases {
		t.Run(fmt.Sprintf("%d * %d", tc.a, tc.b), func(t *testing.T) {
			res := times(tc.a, tc.b)
			if res != tc.ans {
				t.Errorf("%d * %d = %d, expected %d", tc.a, tc.b, res, tc.ans)
			}
		})
	}
}

func TestGcd(t *testing.T) {
	gcdCases := []testCase{
		{42, 56, 14},
		{156, 36, 12},
		{17, 100, 1},
	}
	for _, tc := range gcdCases {
		t.Run(fmt.Sprintf("gcd(%d, %d)", tc.a, tc.b), func(t *testing.T) {
			res := gcd(tc.a, tc.b)
			if res != tc.ans {
				t.Errorf("gcd(%d, %d) = %d, expected %d", tc.a, tc.b, res, tc.ans)
			}
		})
	}
}
